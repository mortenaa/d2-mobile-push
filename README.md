# D2 Mobile Push API

Rest API for mobile apps to manage push subscriptions

## Rest API

Base url for the api: `http://{host}/push/api/v1/`

### List app subscriptions

List available subscriptions for an app:

    GET /app/:appId/subscriptions

`:appId` is an unique id for the app making the request, i.e. `no.tv2.android`.



Response:

```
Status: 200 OK
```
```
#!json
{
  "groups": [
    {
      "name": "Nyhetsvarslinger",
      "subscriptions": [
        {
          "id": 1001,
          "name": "Nyhetsvarsel - TV2 Nyhetene",
          "type": "NEWS"
        },
        {
          "id": 1213,
          "name": "Infovarsel",
          "type": "INFO"
        }
      ]
    },
    {
      "name": "Sportsvarslinger",
      "subscriptions": [
        {
          "id": 1331,
          "name": "Kampvarsel",
          "type": "EVENT",
        }
      ]
    }
  ]
}
```

### List device subscriptions

List active subscriptions for a device:

    GET /app/:appId/device/:deviceId/subscriptions

Response:

```
Status: 200 OK
```
```
#!json
{
  "groups": [
    {
      "name": "Nyhetsvarslinger",
      "subscriptions": [
        {
          "id": 1001,
          "name": "Nyhetsvarsel",
          "type": "NEWS"
        },
      ]
    },
    {
      "name": "Sportsvarsel",
      "subscriptions": [
        {
          "id": 1331,
          "name": "Arsenal - West Ham",
          "type": "EVENT",
          "parameters": {
            "eventId": 1212,
            "expires": "2015-05-22T19:11:11"
          }
        },
        {
          "id": 1331,
          "name": "Stoke - Newcastle",
          "type": "EVENT",
          "parameters": {
            "eventId": 1213,
            "expires": "2015-05-22T19:11:11"
          }
        }
      ]
    }
  ]
}
```

### Create subscription

Create a new subscription:

    POST /app/:appId/device/:deviceId/subscriptions/:subscriptionId

Response:
```
Status: 201 Created
```
```
{
  "id": 1001,
  "name": "Nyhetsvarsel",
  "type": "NEWS"
}
```

### Delete subscription

Delete a subscription:

    DELETE /app/{appId}/device/{deviceId}/subscriptions/{subscriptionId}:

Response:
```
Status: 204 No Content
```

### Create event subscription

Create a new event subscription:

    POST /app/:appId/device/:deviceId/subscriptions/:subscriptionId/event/:eventId

Response:
```
Status: 201 Created
```
```
{
  "id": 1331,
  "name": "Stoke - Newcastle",
  "type": "EVENT",
  "parameters": {
    "eventId": 1213,
    "expires": "2015-05-22T19:11:11"
  }
}
```


### Delete event subscription

Delete a event subscription:

    DELETE /app/{appId}/device/{deviceId}/subscriptions/{subscriptionId}/event/:eventId

Response:
```
Status: 204 No Content
```
