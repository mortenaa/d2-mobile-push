package no.tv2.d2.mobilepush;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootApplication {

    public static void main(final String[] args) {
        SpringApplication.run(BootApplication.class, args);
    }
}
