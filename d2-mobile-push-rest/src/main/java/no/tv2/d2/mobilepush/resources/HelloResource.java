package no.tv2.d2.mobilepush.resources;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import java.util.concurrent.TimeUnit;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author andersfromreide
 */
@RestController
@RequestMapping(value = "/test", produces = MediaType.APPLICATION_JSON)
@Api(value = "/test", description = "Hello world!")
public class HelloResource {

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    @ApiOperation(value = "Get greeting")
    public String hello(@PathVariable String  name) {
        return "Hello " + name ;
    }
}
